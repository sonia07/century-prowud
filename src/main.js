import Vue from 'vue'
import App from './App.vue'
import router from "./router";
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
// import "vue-googlemaps/dist/vue-googlemaps.css";
import * as VueGoogleMaps from 'vue2-google-maps'
import VueCarousel from 'vue-carousel';
import './assets/css/style.min.css'

// Styles
import "./assets/vendors/font-awesome/css/all.css";

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(VueCarousel);
var VueScrollTo = require('vue-scrollto');
Vue.use(VueScrollTo)
Vue.use(VueScrollTo, {
     container: "body",
     duration: 500,
     easing: "ease",
     offset: 0,
     force: true,
     cancelable: true,
     onStart: false,
     onDone: false,
     onCancel: false,
     x: false,
     y: true
 })

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBbBk6yvquBcd65gV0Ethyr-eVxwJllFoc',
    libraries: 'places',
  },
});


new Vue({
  el: '#app',
  router,
  render: h => h(App),
}).$mount('#app')
