import Vue from "vue";
import Router from "vue-router";
// import VueMeta from 'vue-meta';
import Home from "./views/Home.vue";
import Experience from "./views/Experience.vue";
import Contact from "./views/Contact.vue";
import Stockists from "./views/Stockists.vue";
import CreatingResponsibly from "./views/CreatingResponsibly.vue";
import Catalogue from "./views/Catalogue.vue";
import PremiumPlus from "./views/PremiumPlus.vue";
import MDF from "./views/MDF.vue"
import Modal from "./views/Modal.vue"
import Product from "./views/Product.vue"

Vue.use(Router);
// Vue.use(VueMeta);

export default new Router({
  mode: "history",
  linkExactActiveClass: 'is-active',
  routes: [
    // {
    //   path: "/centuryprowud",
    //   name: "home",
    //   component: Home
    // },
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/experience",
      name: "experience",
      component: Experience
    },
    {
      path: "/modal/:slug",
      name: "modal",
      component: Modal
    },
    {
      path: "/product/:slug",
      name: "product",
      component: Product
    },
    {
      path: "/contact",
      name: "contact",
      component: Contact
    },
    {
      path: "/dealers",
      name: "stockists",
      component: Stockists
    },
    {
      path: "/creating-responsibly",
      name: "creating-responsibly",
      component: CreatingResponsibly
    },
    {
      path: "/catalogue",
      name: "catalogue",
      component: Catalogue
    },
    {
      path: "/premium-plus",
      name: "premium-plus",
      component: PremiumPlus
    },
    {
      path: "/mdf",
      name: "mdf",
      component: MDF
    }
  ],
  scrollBehavior() {
    return {
      x: 0,
      y: 0
    };
  }
});
