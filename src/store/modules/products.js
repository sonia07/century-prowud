import * as API_CONFIG from '../config';

const state = {
  products: []
};

const getters = {
  allProducts: state => state.products
};

const actions = {
  async fetchProducts({ commit }) {
    const response = await fetch(API_CONFIG.GET_PRODUCTS)
      .then(res => res.json())
      .then(data => data);
    commit("setProducts", response.data);
  }
};
const mutations = {
  setProducts: (state, products) => (state.products = products)
};

export default {
  state,
  getters,
  actions,
  mutations
};
