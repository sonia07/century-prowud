import API_CONFIG from "../config";
const state = {
  blogs: []
};

const getters = {
  getBlogs: state => state.blogs
};

const actions = {
  async fetchBlogs({ commit }) {
    const response = await fetch(API_CONFIG.GET_BLOGS)
      .then(res => res.json())
      .then(data => data);
    commit("setBlogs", response);
  }
};
const mutations = {
  setBlogs: (state, blogs) => (state.blogs = blogs)
};

export default {
  state,
  getters,
  actions,
  mutations
};
