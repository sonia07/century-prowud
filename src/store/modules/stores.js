import * as API_CONFIG from '../config.js';

const state = {
  ocStores: [],
  jdStores: []
};

const getters = {
  getOCStores: state => state.ocStores,
  getJDStores: state => state.jdStores
};

const actions = {
  async fetchStores({ commit }) {
    const response = await fetch(API_CONFIG.GET_STORES)
      .then(res => res.json())
      .then(data => data);
      let jdData = [],
        ocData = [];
      if (response.status_code == 200) {
          jdData.push(response.data.jaquar_dealer);
          ocData.push(response.data.orientation_center);
        commit("setOCStores", { ocData, success: true });
        commit("setJDStores", { jdData, success: true });
      }
  }
};
const mutations = {
  setOCStores: (state, stores) => (state.ocStores = stores),
  setJDStores: (state, stores) => (state.jdStores = stores)
};

export default {
  state,
  getters,
  actions,
  mutations
};
