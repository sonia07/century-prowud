import API_CONFIG from "../config";
const state = {
  print_media: [],
  digital_media: []
};

const getters = {
  getPrintMedia: state => state.print_media,
  getDigitalMedia: state => state.digital_media
};

const actions = {
  async fetchAllMedia({ commit }) {
    const response = await fetch(API_CONFIG.GET_MEDIA)
      .then(res => res.json())
      .then(data => data);
    let digiMedia = [],
      printMedia = [];
    if (response.status_code == 200) {
      response.data.forEach(res => {
        if (res.mediatype == "Digital") {
          digiMedia.push(res);
        } else if (res.mediatype == "Print") {
          printMedia.push(res);
        }
      });
      commit("setPrintMedia", { printMedia, success: true });
      commit("setDigitalMedia", { digiMedia, success: true });
    } else {
      commit("setPrintMedia", { printMedia, success: false });
      commit("setDigitalMedia", { digiMedia, success: false });
    }
  }
};
const mutations = {
  setPrintMedia: (state, media) => (state.print_media = media),
  setDigitalMedia: (state, media) => (state.digital_media = media)
};

export default {
  state,
  getters,
  actions,
  mutations
};
