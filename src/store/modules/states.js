import * as API_CONFIG from '../config.js';

const state = {
  states: []
};

const getters = {
  allStates: state => state.states
};

const actions = {
  async fetchStates({ commit }) {
    const response = await fetch(API_CONFIG.GET_STATES)
    .then(res => res.json())
    .then(data => data);
    let states = []
    if (response.status_code == 200) {
      $.each(response.data.state, function(key, value){
        states.push(value);
      });
      commit("setStates", { states, success: true });
    }
  }
};
const mutations = {
  setStates: (state, states) => (state.states = states)
};

export default {
  state,
  getters,
  actions,
  mutations
};
