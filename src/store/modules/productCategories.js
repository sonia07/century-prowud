import * as API_CONFIG from '../config.js';

const state = {
  range: []
};

const getters = {
  allCategories: state => state.range
};

const actions = {
  async fetchCategories({ commit }) {
    const response = await fetch(API_CONFIG.GET_PRODUCT_CATEGORIES)
      .then(res => res.json())
      .then(data => data);
    commit("setCategories", response.data);
  }
};
const mutations = {
  setCategories: (state, categories) => (state.range = categories)
};

export default {
  state,
  getters,
  actions,
  mutations
};
