import Vue from "vue";
import Vuex from "vuex";
import productCategories from "./modules/productCategories";
import products from "./modules/products";
import stores from "./modules/stores";
import states from "./modules/states";
import media from "./modules/media";
import blogs from "./modules/blogs";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    productCategories,
    products,
    stores,
    states,
    media,
    blogs
  }
});
