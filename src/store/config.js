
const API_BASE_URL = 'http://13.233.226.66/century-admin/public/api/';

module.exports =  {
    GET_catalogues : API_BASE_URL + 'catalogues',
    GET_cataloguesdetails : API_BASE_URL + 'cataloguedetails',
    GET_allproducts : API_BASE_URL + 'products',
    GET_allcategories : API_BASE_URL + 'categories',
    GET_allmodals : API_BASE_URL + 'modals',
    GET_products : API_BASE_URL + 'products',
    Contact : API_BASE_URL + 'contact',
}
